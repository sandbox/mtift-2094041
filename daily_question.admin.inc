<?php

/**
 * Provides a configuration form for a daily question entity.
 */
function daily_question_form($form, &$form_state, $question = NULL) {
  $form['question_date'] = array(
    '#title' => t('Question date'),
    '#description' => t("The date on which this question should appear."),
    '#date_timezone' => 'America/Chicago',
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#default_value' => isset($question->question_date) ? date('Y-m-d', $question->question_date) : '',
  );
  $form['question_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Question text.'),
    '#default_value' => isset($question->question_text) ? check_plain($question->question_text) : '',
  );
  // Basic campaign information
  $form['answers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Possible Answers'),
  );
  $form['answers']['answer_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Answer 1'),
    '#description' => t('The first multiple-choice answer for this question.'),
    '#default_value' => isset($question->answer_1) ? check_plain($question->answer_1) : '',
  );
  $form['answers']['answer_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Answer 2'),
    '#description' => t('The second multiple-choice answer for this question.'),
    '#default_value' => isset($question->answer_2) ? check_plain($question->answer_2) : '',
  );
  $form['answers']['answer_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Answer 3'),
    '#description' => t('The third multiple-choice answer for this question.'),
    '#default_value' => isset($question->answer_3) ? check_plain($question->answer_3) : '',
  );
  $form['correct_answer'] = array(
    '#type' => 'select',
    '#title' => t('Correct Answer'),
    '#description' => t('The correct multiple-choice answer for this question.'),
    '#options' => array(
      1 => t('Answer 1'),
      2 => t('Answer 2'),
      3 => t('Answer 3'),
    ),
  );
  $form['read_more'] = array(
    '#type' => 'textfield',
    '#title' => t('Read More'),
    '#description' => t('URL for more information on Daily Question answers.'),
    '#default_value' => isset($question->read_more) ? check_plain($question->read_more) : '',
  );
  $form['correct_answers'] = array(
    '#markup' => get_correct_answers($question),
    '#title' => t('Correct Answers'),
    '#description' => t('The list of users who answered this question correctly.'),
  );
  $form['incorrect_answers'] = array(
    '#markup' => get_incorrect_answers($question),
    '#title' => t('Incorrect Answers'),
    '#description' => t('The list of users who answered this question incorrectly.'),
  );

  field_attach_form('campaign', $question, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($question->id) ? t('Update Question') : t('Save Question'),
    ),
  );
  return $form;
}

/**
 * Creates a list of users that have correctly answered a daily question.
 */
function get_correct_answers($question) {
  $output = t('<b>Correct Answers</b>');
  // Gather a list of the users who answered correctly
  if (!empty($question->correct_answers)) {
    $correct_users = explode(",", $question->correct_answers);
    $items = array();
    foreach ($correct_users as $correct_uid) {
      $correct_user = user_load($correct_uid);
      $items['items'][] = l($correct_user->name, 'user/' . $correct_user->uid);
    }
    $output .= theme('item_list', $items);
    $output .= t('<br />');
  }
  // Provide default text when there are no incorrect answers
  else {
    $output .= t('<p>None</p>');
  }
  return $output;
}

/**
 * Creates a list of users that have incorrectly answered a daily question.
 */
function get_incorrect_answers($question) {
  $output = t('<b>Incorrect Answers</b>');
  if (!empty($question->incorrect_answers)) {
    $incorrect_users = explode(",", $question->incorrect_answers);
    $items = array();
    foreach ($incorrect_users as $incorrect_uid) {
      $incorrect_user = user_load($incorrect_uid);
      $items['items'][] = l($incorrect_user->name, 'user/' . $incorrect_user->uid);
    }
    $output .= theme('item_list', $items);
    $output .= t('<br />');
  }
  // Provide default text when there are no incorrect answers
  else {
    $output .= t('<p>None</p>');
  }
  return $output;
}

/**
 * Provides a validate function for daily questions.
 */
function daily_question_form_validate($form, &$form_state) {
  // The read_more field is a URL
  $url = $form_state['values']['read_more'];
  // Provide basic URL checking rather than require a module that does it better
  $pattern = '((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)';
  $valid = preg_match($pattern, $url);
  if ($valid == FALSE) {
   form_set_error('read_more', 'The Read More link must be a valid URL');
  }
}

/**
 * Provides a submit function for daily questions.
 */
function daily_question_form_submit($form, &$form_state) {
  $question = entity_ui_form_submit_build_entity($form, $form_state);
  $question->question_date = strtotime($form_state['values']['question_date']);
  $question->save();
  drupal_set_message(t('The question "@question" has been saved', array('@question' => $question->question_text)));
  $form_state['redirect'] = 'admin/structure/daily-questions';
}
