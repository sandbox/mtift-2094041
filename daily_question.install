<?php

/**
 * @file
 * Install and uninstall functions for the Daily Question module.
 */

/**
 * Implements hook_install().
 */
function daily_question_install() {
  $t = get_t();
  drupal_set_message($t("Daily Question settings are available at !link",
    array( '!link' => l(t('admin/daily-questions'),  'admin/daily-questions'))
  ));
}

/**
 * Implements hook_schema().
 */
function daily_question_schema() {
  $schema['daily_question'] = array(
    'description' => 'Stores daily question data.',
    'fields' => array(
      'id' => array(
        'description' => 'Primary key for the daily question entity.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'question_date' => array(
        'description' => 'The date on which the question should appear',
        'type' => 'int',
        'length' => 10,
        'not null' => FALSE,
      ),
      'question_text' => array(
        'description' => 'Question text.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'answer_1' => array(
        'description' => 'The first multiple-choice answer for this question.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'answer_2' => array(
        'description' => 'The second multiple-choice answer for this question.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'answer_3' => array(
        'description' => 'The third multiple-choice answer for this question.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'correct_answer' => array(
        'description' => 'The correct multiple-choice answer for this question.',
        'type' => 'int',
        'length' => 1,
        'not null' => FALSE,
      ),
      'read_more' => array(
        'description' => 'URL for more information on Daily Question answers.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'correct_answers' => array(
        'description' => 'The list of users who answered this question correctly.',
        'type' => 'text',
        'size' => 'medium',
      ),
      'incorrect_answers' => array(
        'description' => 'The list of users who answered this question incorrectly.',
        'type' => 'text',
        'size' => 'medium',
      ),
      'created' => array(
        'description' => 'Date and time the question was created.',
        'type' => 'int',
        'length' => 10,
        'not null' => FALSE,
      ),
      'updated' => array(
        'description' => 'Date and time the question was last modified.',
        'type' => 'int',
        'length' => 10,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}


